/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbds.servlet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public class EtudiantsServlet extends HttpServlet {

    private static final String VUE = "etudiants.html";
    private static final String FILE_NAME = "etudiants.csv";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect(VUE);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        if (request.getParameterMap().containsKey("myList")) {
            String testAction = request.getParameter("myList");
            if (testAction.equals("list")) {
                if (CheckFileExists(FILE_NAME)) {

                    BufferedReader br = new BufferedReader(new FileReader(FILE_NAME));
                    String line;

                    String title = "Liste des etudiants";
                    out.println("<HTML>");
                    out.println("<HEAD>");
                    out.println("<TITLE>" + title + "</TITLE>");
                    out.println("</HEAD>");

                    out.println("<BODY>");

                    //1er Tableau pour la redirection vers le formulaire
                    out.println("<CENTER>");
                    out.println("<TABLE BORDER=1 CELLPADDING=0 CELLSPACING=0 WIDTH=50%>");

                    out.println("<TR>");
                    out.println("<TD><CENTER><B>");
                    out.println("<a href='http://localhost:8080/TP_01_HYPPOLITE_RICKERSON/etudiants'>AJOUTER UN ETUDIANT</a>");
                    out.println("</TABLE>");
                    out.println("<BR>");

                    //2eme Tableau d'affichage de la liste des etudiants
                    out.println("<CENTER>");
                    out.println("<TABLE BORDER=1 CELLPADDING=0 CELLSPACING=0 WIDTH=70%>");

                    out.println("<TR>");
                    out.println("<TD><CENTER><B>");
                    out.println("<FONT SIZE=+2>NOM</FONT>");
                    out.println("</B></CENTER></TD>");
                    out.println("<TD><CENTER><B>");
                    out.println("<FONT SIZE=+2>PRENOM</FONT>");
                    out.println("</B></CENTER></TD>");
                    out.println("<TD><CENTER><B>");
                    out.println("<FONT SIZE=+2>EMAIL</FONT>");
                    out.println("</B></CENTER></TD>");

                    out.println("</TR>");

                    while ((line = br.readLine()) != null) {
                        // use comma as separator 
                        String[] cols = line.split(",");
//
                        out.println("<TR><td>" + cols[0] + "</td><td>" + cols[1] + "</td><td>" + cols[2] + "</td></TR>");

                    }

                    out.println("</TABLE>");
                    out.println("</CENTER>");
                    out.println("</BODY></HTML>");
                } else {

                    out.println("<html>");
                    out.println("<head>");
                    out.println("<title>Test</title>");
                    out.println("<meta charset='UTF-8'>");
                    out.println("<meta name='viewport' content='width=device-width, initial-scale=1.0'>");
                    out.println("</head>");
                    out.println("<body>");
                    out.println("<div>Il n' y a pas d'etudiants dans liste</div>");
                    out.println("<a href='http://localhost:8080/TP_01_HYPPOLITE_RICKERSON/etudiants'>AJOUTER UN ETUDIANT</a>");
                    out.println(" </body>");
                    out.println("</html>");

                }
            }
        } else {
            processRequest(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String nom = request.getParameter("nom");
        String prenom = request.getParameter("prenom");
        String email = request.getParameter("email");

        try {
            PrintWriter out = response.getWriter();
            String filename = "etudiants.csv";
            FileWriter fw = new FileWriter(filename, true);

            fw.append(nom);
            fw.append(',');
            fw.append(prenom);
            fw.append(',');
            fw.append(email);
            fw.append('\n');

            fw.flush();
            fw.close();
            out.println("<b>Csv file Successfully created.</b>");

        } catch (IOException ex) {
        }
        response.sendRedirect("http://localhost:8080/TP_01_HYPPOLITE_RICKERSON/etudiants?myList=list");

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public static Boolean CheckFileExists(String _fileName) {
        // test to see if a file exists
        File file = new File(_fileName);
        return file.exists();
    }

}
